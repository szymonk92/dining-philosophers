/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monpie.diningphilosophers;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Szymon
 */
public class Fork {

    private final int name;

    public Fork(int name) {
        this.name = name;
    }

    ReentrantLock reentrantLock = new ReentrantLock();

    boolean pickUp(Philosopher philosopher, String fork) throws InterruptedException {

        if (reentrantLock.tryLock(10, TimeUnit.MILLISECONDS)) {
            System.out.println("Philosopher " + philosopher.getName() + " podniosl " + this);
            return true;
        }
        return false;

    }

    void drop(Philosopher philosopher, String fork) {
        reentrantLock.unlock();
        System.out.println("Filozof " + philosopher.getName() + " odlozyl " + this);
    }

    @Override
    public String toString() {
        return "Widelec " + name;
    }

}
