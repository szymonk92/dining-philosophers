/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monpie.diningphilosophers;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Szymon
 */
public class Main {

    private static final int NUMBER_OF_PHILOSOPHERS = 5;

    private static final int TIME_FOR_DINNER = 24000;

    public static void main(String[] args) throws InterruptedException {

        List<Philosopher> philosophers = Lists.newArrayList();
        List<Fork> forks = Lists.newArrayList();
        ExecutorService executorService = null;

        try {
            for (int i = 0; i < NUMBER_OF_PHILOSOPHERS; i++) {
                forks.add(new Fork(i));
            }

            executorService = Executors.newFixedThreadPool(NUMBER_OF_PHILOSOPHERS);
            for (int i = 0; i < NUMBER_OF_PHILOSOPHERS; i++) {
                philosophers.add(new Philosopher(i, forks.get(i), forks.get((i + 1) % NUMBER_OF_PHILOSOPHERS)));
                executorService.execute(philosophers.get(i));
            }

            Thread.sleep(TIME_FOR_DINNER);
            for (Philosopher philosopher : philosophers) {
                philosopher.timeToFinishMeal = true;
            }

        } finally {
            executorService.shutdown();

            while (!executorService.isTerminated()) {
                Thread.sleep(1000);
            }

            int sumOfMeals = 0;
            for (Philosopher philosopher : philosophers) {
                System.out.println("Filozof " + philosopher + " zjadl "
                        + philosopher.getNoOfMeals() + " posilkow");
                sumOfMeals += philosopher.getNoOfMeals();
            }
            System.out.println("Liczba wszystkich posilkow: " + sumOfMeals);

        }

    }
}
