/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monpie.diningphilosophers;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Szymon
 */
public class Philosopher implements Runnable {

    private final int name;
    private final Fork leftFork;
    private final Fork rightFork;
    private int numberOfMeals = 0;

    volatile boolean timeToFinishMeal = false;
    Random random = new Random();

    public Philosopher(int name, Fork leftFork, Fork rightFork) {
        this.name = name;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    public int getName() {
        return name;
    }

    public void think() throws InterruptedException {
        System.out.println(this + " mysli.");
        Thread.sleep(random.nextInt(1000));
    }

    public void eat() throws InterruptedException {
        System.out.println(this + " je.");
        Thread.sleep(random.nextInt(1000));
        numberOfMeals++;
    }

    public int getNoOfMeals() {
        return numberOfMeals;
    }

    @Override
    public String toString() {
        return "Filozof " + name;
    }

    @Override
    public void run() {

        while (!timeToFinishMeal) {
            try {
                think();
                if (leftFork.pickUp(this, "lewy")) {
                    if (rightFork.pickUp(this, "prawy")) {
                        eat();
                        rightFork.drop(this, "prawy");
                    }
                    leftFork.drop(this, "lewy");
                }

            } catch (InterruptedException ex) {
                Logger.getLogger(Philosopher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
